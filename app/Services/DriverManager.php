<?php

namespace App\Services;


use App\Services\Api\Drivers\FourSquare;
use GuzzleHttp\Client;
use Illuminate\Support\Manager;

class DriverManager extends Manager
{
    /**
     * Get the default driver name.
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        return $this->createFourSquareDriver();
    }

    /**
     * Create an instance of the Foursquare driver
     *
     * @return FourSquare
     */
    protected function createFourSquareDriver()
    {
        $provider = new FourSquare(new Client());

        return $provider;
    }
}
