<?php

namespace App\Services;

use App\Services\DriverManager;

class Api
{
    /**
     * @var null
     */
    private $driver;

    /**
     * Api constructor.
     *
     * @param null $driver
     */
    public function __construct($driver = null)
    {
        $this->driver = $driver;
    }

    /**
     * Driver
     *
     * @param $driver
     *
     */
    public function driver($driver)
    {
        $manager = new DriverManager(app());
        $this->driver = $manager->driver($driver);

        return $this->driver;
    }


    /**
     * Send
     *
     * @param $url
     * @param array $parameters
     * @return mixed
     */
    public function send($url, $parameters = [])
    {
        return $this->driver->send($url, $parameters);
    }
}
