<?php

namespace App\Services\Api\Drivers;

interface DriverInterface
{
    /**
     * Send
     *
     * @param string $url
     * @param array $parameters
     * @return mixed
     */
    public function send(string $url = '', array $parameters = []);
}
