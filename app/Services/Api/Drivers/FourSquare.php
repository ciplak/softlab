<?php

namespace App\Services\Api\Drivers;

use Illuminate\Support\Facades\Log;

class FourSquare extends AbstractDriver implements DriverInterface
{

    /**
     * Send
     *
     * @param string $url
     * @param array $parameters
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function send($url = '', $parameters = [])
    {
        try {
            $url = $url . '?' . http_build_query($parameters);

            $response = $this->client->get($url);
            $response = json_decode($response->getBody());

//            Log::info($response);

            return $response;

        } catch (\Exception $exception) {
//            Log::error($exception->getMessage());
        }
    }
}
