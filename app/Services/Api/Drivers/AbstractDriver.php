<?php

namespace App\Services\Api\Drivers;

use GuzzleHttp\Client;

abstract class AbstractDriver
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * AbstractDriver constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }
}
