<?php

namespace App\Http\Controllers;

use App\Services\Api;
use Illuminate\Http\Request;

class FourSquareController extends Controller
{
    const FOUR_SQUARE_CLIENT_ID = '2LRVIIZAHIZBFTPW0JUZM0QTCPZFB1DH3FNTCRHWZI12WP4D';
    const FOUR_SQUARE_CLIENT_SECRET = 'WNPDOJTA5JINEZX50Y2ANKGUKOH0N2CMJPWXC2ZIPPF5DHZD';
    private $fourSquareClientId;
    private $fourSquareClientSecret;
    private $date;

    /**
     * FourSquareController constructor.
     */
    public function __construct()
    {
        $this->date = date('Ymd');
        $this->fourSquareClientId = self::FOUR_SQUARE_CLIENT_ID; // TODO move to env file env('FOURSQUARE_CLIENT_ID');
        $this->fourSquareClientSecret = self::FOUR_SQUARE_CLIENT_SECRET; // TODO move to env file env('FOURSQUARE_CLIENT_SECRET');
    }

    /**
     * Categories
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function categories()
    {
        $formParameters = [
            'client_id' => $this->fourSquareClientId,
            'client_secret' => $this->fourSquareClientSecret,
            'v' => $this->date,
        ];


        $url = 'https://api.foursquare.com/v2/venues/categories';
        $categories = $this->sendRequest($url, $formParameters)->response;

        return view('welcome', compact('categories'));
    }

    /**
     * Detail
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail()
    {
        $formParameters = [
            'near' => 'valetta',
            'query' => request('categoryName'),
            'client_id' => $this->fourSquareClientId,
            'client_secret' => $this->fourSquareClientSecret,
            'v' => $this->date,
        ];

        $url = 'https://api.foursquare.com/v2/venues/explore';
        $detail = $this->sendRequest($url, $formParameters)->response;

        return view('detail', compact('detail'));
    }

    /**
     * Send Request
     *
     * @param $url
     * @param $formParameters
     * @return mixed
     */
    private function sendRequest($url, $formParameters)
    {
         return (new Api())
            ->driver('FourSquare')
            ->send($url, $formParameters);
    }
}
