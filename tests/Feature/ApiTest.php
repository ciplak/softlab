<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ApiTest extends TestCase
{
    /**
     * Feature Test Categories
     *
     * @return void
     */
    public function testCategories()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * Feature Test Detail
     *
     * @return void
     */
    public function testDetail()
    {
        $url = route('detail', ['categoryName' => 'Cafe']);
        $response = $this->get($url);

        $response->assertStatus(200);
    }
}
