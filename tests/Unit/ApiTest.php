<?php

namespace Tests\Unit;

use App\Services\Api;
use App\Services\Api\Drivers\AbstractDriver;
use App\Services\Api\Drivers\DriverInterface;
use App\Services\Api\Drivers\FourSquare;
use App\Services\DriverManager;
use Mockery;
use PhpParser\Node\Stmt\For_;
use PHPUnit\Framework\TestCase;

class ApiTest extends TestCase
{

    public function testDriverForSquare()
    {
        $mock = Mockery::mock(FourSquare::class);

        $this->assertInstanceOf(FourSquare::class, $mock);
    }

    public function testAbstractDriver()
    {
        $mock = Mockery::mock(AbstractDriver::class);

        $this->assertInstanceOf(AbstractDriver::class, $mock);
    }

    public function testDriverInterface()
    {
        $mock = Mockery::mock(DriverInterface::class);

        $this->assertInstanceOf(DriverInterface::class, $mock);
    }

    public function testDriverManager()
    {
        $mock = Mockery::mock(DriverManager::class);

        $this->assertInstanceOf(DriverManager::class, $mock);

        $response = $mock->makePartial()
            ->shouldReceive('getDefaultDriver')
            ->andReturn(FourSquare::class);

        $this->isInstanceOf(FourSquare::class, $response);

    }

}
