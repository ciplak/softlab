@if(isset($detail))

    <a href="/"> Back to List</a><br/><br/>
    Query : {{$detail->query}} <br/><br/>
    Total Results : {{$detail->totalResults}} <br/><br/>
    @if($detail->totalResults > 0) Items : <br/><br/> @endif
    @foreach($detail->groups as $groups)
        @foreach($groups->items as $items)
            {{$items->venue->name}} <br/>
        @endforeach
    @endforeach
@endif
