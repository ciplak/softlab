<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <link href="css/custom.css" rel="stylesheet">
    <style>
        .ui-menu {
            width: 300px;
        }
    </style>

</head>
<body>

<ul id="menu">
    @foreach($categories as $category)
        @foreach($category as $item)
            <li>
                <div>{{$item->name}} <span style="float:right"> > </span></div>
                <ul>
                    <li>
                        @foreach($item->categories as $subCategory)
                            <div>
                                <a href="{{route('detail', ['categoryName' => $subCategory->name])}}">{{$subCategory->name}}</a>
                            </div>
                        @endforeach
                    </li>
                </ul>
            </li>
        @endforeach
    @endforeach
</ul>

<script src="js/vendor.js"></script>

<script>
    $(function () {
        $("#menu").menu();
    });
</script>
<script>
</script>

</body>
</html>









